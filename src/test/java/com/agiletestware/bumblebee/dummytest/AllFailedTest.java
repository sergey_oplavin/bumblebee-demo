package com.agiletestware.bumblebee.dummytest;

import org.junit.Assert;
import org.junit.Test;

public class AllFailedTest {

	@Test
	public void testFail1() {
		Assert.fail("Test 1 failed");
	}

	@Test
	public void testFail2() {
		Assert.fail("Test 2 failed");
	}
	
	@Test
	public void testFail3() {
		throw new RuntimeException("Test 3 failed with exception");
	}

	@Test
	public void testFail4() {
		Assert.fail("Test 4 failed");
	}

	@Test
	public void testFail5() {
		throw new RuntimeException("Test 5failed with exception");
	}
}
