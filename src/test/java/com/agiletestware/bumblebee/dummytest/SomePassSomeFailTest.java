package com.agiletestware.bumblebee.dummytest;

import org.junit.Assert;
import org.junit.Test;

public class SomePassSomeFailTest {

	@Test
	public void test1() {
		Assert.fail("test1 failed");
	}

	@Test
	public void test2() {
		System.out.println("test2 passed");
	}

	@Test
	public void test3() {
		System.out.println("test3 passed");
	}

	@Test
	public void test4() {
		Assert.fail("test4 failed");
	}

	@Test
	public void test5() {
		System.out.println("test5 passed");
	}
}
